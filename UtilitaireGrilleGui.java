import java.awt.Color;
import java.util.*;

public class UtilitaireGrilleGui {

	public static void remplirGuiDepart(int[][] grille, GrilleGui gui, double nbPourcentage) {

    double nbr_case = (gui.getNbLignes() * gui.getNbColonnes())*nbPourcentage;
    int i=0;

    while (i < nbr_case) {
		
      Random rand = new Random();

      int ligne = rand.nextInt(gui.getNbLignes());
      int colonne = rand.nextInt(gui.getNbColonnes());

      if (gui.getValeur(ligne,colonne).equals(" ")) {

        int binary = grille[ligne][colonne];
        gui.setValeur(ligne, colonne, Integer.toString(binary));
        i++;
      }
    }
}

	public static int getNbValeursValide(int[][] grille, GrilleGui gui) {
		
		return 0;
	}

	public static int obtenirErreurs(GrilleGui gui) {
		
    int lignes = gui.getNbLignes();
    int colonnes = gui.getNbColonnes();

    for ( int l=0; l < lignes; l++ ) {
      for ( int c=0; c < colonnes; c++ ) {
        int valeur=valeurGuiConvertitEnInt(gui.getValeur(l,c));
        int bottom=0; int top=0; int right=0; int left=0;

        if ( c == 0 ) {
          right = valeurGuiConvertitEnInt(gui.getValeur(l,c+1));
        } else if ( l == lignes-1 ) {
          left = valeurGuiConvertitEnInt(gui.getValeur(l,c-1));
        } else {
          left=valeurGuiConvertitEnInt(gui.getValeur(l,c-1));
          right=valeurGuiConvertitEnInt(gui.getValeur(l,c+1));
        }

        if ( l == 0 ) {
          bottom = valeurGuiConvertitEnInt(gui.getValeur(l+1,c));
        } else if ( c == colonnes-1 ) {
          top = valeurGuiConvertitEnInt(gui.getValeur(l-1,c));
        } else {
          top = valeurGuiConvertitEnInt(gui.getValeur(l-1,c));
          bottom = valeurGuiConvertitEnInt(gui.getValeur(l+1,c));
        }

        if ( l==lignes-1 && valeur==top && valeur==left && valeur==right ) {
          return 0;
        } else if ( valeur==bottom && l==0 && valeur==left && valeur==right ) {
          return 0;
        } else if ( valeur==bottom && valeur==top && c==0 && valeur==right ) {
          return 0;
        } else if ( valeur==bottom && valeur==top && valeur==left && c==colonnes-1 ) {
          return 0;
        } else if ( valeur==bottom && valeur==top && valeur==left && valeur==right ) {
          return 0;
        }
      }
    }

		return 1;
	}

	public static boolean grilleEstPleine(GrilleGui gui) {

    int lignes = gui.getNbLignes();
    int colonnes = gui.getNbColonnes();

    for ( int l=0; l < lignes; l++ ) {
      for ( int c=0; c < colonnes; c++ ) {
        if (gui.getValeur(l,c).equals(" ")) {
          return false;
        }
      }
    }
		
		return true;
	}

	public static void reinitialiserCouleur(GrilleGui gui) {
		
    int lignes = gui.getNbLignes();
    for ( int i=0; i < lignes; i++ ) {
      setCouleurFondLigne(gui, i, Constantes.COULEUR_FOND);
    }
		
	}

	public static void afficherChangementGrilleGui(GrilleGui gui) {

	}

  // Permet de changer le fond d'une ligne entiere
  public static void setCouleurFondLigne(GrilleGui gui, int ligne, Color couleurFond) {

    int nbColonnes = gui.getNbColonnes();

    for ( int colonne=0; colonne < nbColonnes; colonne++ ) {
		  gui.setCouleurFond(ligne,colonne,couleurFond);
    }
  }

  // Permet de changer le fond d'une colonne entiere
  public static void setCouleurFondColonne(GrilleGui gui, int colonne, Color couleurFond) {

    int nbLignes = gui.getNbLignes();

    for ( int ligne=0; ligne < nbLignes; ligne++ ) {
		  gui.setCouleurFond(colonne,ligne,couleurFond);
    }
  }

  public static boolean checkLigneIdentique( GrilleGui gui, int ligneFixe ) {

    int nbLignes = gui.getNbLignes();
    int nbColonnes = gui.getNbColonnes();

    for ( int ligne=0; ligne < nbLignes; ligne++ ) {
      int colonne = 0;
      while ( colonne < nbColonnes && gui.getValeur(ligne,colonne).equals(gui.getValeur(ligneFixe,colonne)) && ligneFixe!=ligne ) {
        colonne++;
      }
      if ( colonne == nbColonnes ) {
        return true;
      }
    }

    return false;
  }
	
  public static boolean checkColonneIdentique( GrilleGui gui, int colonneFixe ) {

    int nbLignes = gui.getNbLignes();
    int nbColonnes = gui.getNbColonnes();

    for ( int colonne=0; colonne < nbColonnes; colonne++ ) {
      int ligne = 0;
      while ( ligne < nbLignes && gui.getValeur(ligne,colonne).equals(gui.getValeur(ligne,colonneFixe)) && colonneFixe!=colonne ) {
        ligne++;
      }
      if ( ligne == nbLignes ) {
        return true;
      }
    }

    return false;
  }

	public static void ajusterGui(GrilleGui gui) {
		
    int matchingCount=0;
    int lignes = gui.getNbLignes();
    int colonnes = gui.getNbColonnes();

    // Match lignes identiques
    for ( int l=0; l < lignes; l++ ) {

      if ( checkLigneIdentique(gui,l) ) {
        setCouleurFondLigne(gui, l, Constantes.COULEUR_LIGNE_IDENTIQUE);
      }
    }

    // Match colonnes identiques
    for ( int c=0; c < colonnes; c++ ) {

      if ( checkColonneIdentique(gui,c) ) {
        setCouleurFondColonne(gui, c, Constantes.COULEUR_COLONNE_IDENTIQUE);
      }
    }

	}


	public static void remettreGrilleDepart(int[][] grille, GrilleGui gui) {
		
    int lignes = gui.getNbLignes();
    int colonnes = gui.getNbColonnes();

    for ( int l=0; l < lignes; l++ ) {
      for ( int c=0; c < colonnes; c++ ) {
        grille[l][c]=valeurGuiConvertitEnInt(gui.getValeur(l,c));
      }
    }
	}

	public static int valeurGuiConvertitEnInt(String valeur) {
		
		return Integer.parseInt(valeur);
	}

}
